class Code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  PEGS = { "r" => :red, "o" => :orange, "y" => :yellow, "g" => :green,
          "b" => :blue, "p" => :purple }

  def self.random
    colors = PEGS.keys
    random = Hash[colors.sample(4).zip([1, 2, 3, 4])]
    Code.new(random)
  end

  def exact_matches(user_input)
    matches = 0
    (0..3).each {|i| matches += 1 if pegs[i] == user_input.pegs[i]}
    matches
    #
  end

  def near_matches(user_input)
    near_matches = 0
       peg_code = @pegs
       user_input.pegs.each_with_index do |x, y|
         if peg_code.include?(x) && self[y] != user_input[y]
           near_matches += 1
           peg_code.slice!(peg_code.index(x))
         end
       end
       near_matches
  end

  def [](index)
    pegs[index]
  end

  def self.parse(user_input)
    user_input.downcase!
    raise "Input is incorrect!" if user_input.split("").any? {|el| !PEGS.key?(el)}
    key = []
    user_input.split("").each { |el| key << PEGS[el] }
    Code.new(key)
  end

  def ==(code)
   return false unless code.is_a?(Code)
   self.pegs === code.pegs
  end

end


#=====================================================================================================

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil)
     secret_code.nil? ? @secret_code = Code.random : @secret_code = secret_code
  end

 def get_guess
    puts "Pick four colors..."
    Code.parse(gets.chomp)
  end

  def display_matches(user_input)
    puts "exact matches: #{secret_code.exact_matches(user_input)}"
    puts "near matches: #{secret_code.near_matches(user_input)}"
  end

end
